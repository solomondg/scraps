package com.team2898.robot.test

import com.team2898.engine.math.linear.Matrix
import com.team2898.engine.math.linear.T
import com.team2898.engine.math.linear.dim
import com.team2898.engine.math.linear.row
import com.team2898.engine.test.test.points
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import jaci.pathfinder.Pathfinder
import jaci.pathfinder.Pathfinder.d2r
import jaci.pathfinder.Trajectory
import jaci.pathfinder.Waypoint
import kotlin.math.round
import kotlin.math.roundToInt
import kotlin.system.measureTimeMillis


class SSTest : StringSpec() {

    val reference = Matrix(Matrix(arrayOf(row(3.0, 3.0))).T.data)

    init {
        println(points[0])
        //Drivetrain.u = Matrix(arrayOf(row(12.0, 12.0))).T
        for (i in 0..(5 / Drivetrain.dt).roundToInt()) {
            val state = Drivetrain.step(Drivetrain.genU(reference))
            println("${state.time}: V:${state.vel.dx}, W:${state.vel.dtheta}")
        }
    }
}