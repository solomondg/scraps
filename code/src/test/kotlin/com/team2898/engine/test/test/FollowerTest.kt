package com.team2898.engine.test.test

import com.team2898.engine.kinematics.RigidTransform2d
import com.team2898.engine.kinematics.Rotation2d
import com.team2898.engine.kinematics.Translation2d
import com.team2898.engine.kinematics.Twist2d
import com.team2898.engine.math.linear.Matrix
import com.team2898.engine.math.linear.T
import com.team2898.engine.math.linear.row
import com.team2898.robot.test.Drivetrain
import io.kotlintest.specs.StringSpec
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D
import java.io.File


class FollowerTest : StringSpec() {

//    val ref = Matrix(Matrix(arrayOf(row(path[currentIndex]., path[currentIndex].y))).T.data)

    // pathfinder gives us: x ref, y ref, +v ref, heading ref
    // x ref, y ref, v ref, w ref

    init {
        val path = points
        val sb = StringBuilder()
        val size = path.size

        var curretTime = 0.0
        var currentIndex = 0

//        b=0.18 z=0.7

        while (size > currentIndex) {
            break
            val target = path[currentIndex]
            val ramsete_ref = ramsete(
                    positionReference = RigidTransform2d(Translation2d(target.x, target.y), Rotation2d.createFromRadians(target.heading)),
                    velocityReference = Twist2d(target.vel, 0.0, target.angVel),
                    pose = Drivetrain.state.pose,
                    b = 0.18,
                    zeta = 0.7
            )
            val ref = Matrix(Matrix(arrayOf(row(ramsete_ref.first, ramsete_ref.second))).T.data)
            val state = Drivetrain.step(Drivetrain.genU(ref))
            if (currentIndex + 1 == size) break
            if (path[currentIndex + 1].t <= curretTime) currentIndex++
            curretTime += Drivetrain.dt
        }

    }
}
