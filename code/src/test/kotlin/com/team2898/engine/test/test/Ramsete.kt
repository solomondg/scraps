package com.team2898.engine.test.test

import com.team2898.engine.extensions.Vector2D.dot
import com.team2898.engine.extensions.Vector2D.minus
import com.team2898.engine.extensions.Vector2D.unaryMinus
import com.team2898.engine.kinematics.RigidTransform2d
import com.team2898.engine.kinematics.Rotation2d
import com.team2898.engine.kinematics.Translation2d
import com.team2898.engine.kinematics.Twist2d
import com.team2898.engine.math.sinc
import com.team2898.engine.motion.DriveSignal
import com.team2898.robot.test.Drivetrain
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D
import java.lang.Math.pow
import kotlin.math.acos
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

/*
def ramsete(pose_desired, v_desired, omega_desired, pose, b, zeta):
    e = pose_desired - pose
    e.rotate(-pose.theta)

    k = 2 * zeta * math.sqrt(omega_desired ** 2 + b * v_desired ** 2)
    v = v_desired * math.cos(e.theta) + k * e.x


    return v, omega
 */

// Twist: x, y, theta
//        v     w

fun ramsete(
        positionReference: RigidTransform2d,
        velocityReference: Twist2d,
        pose: RigidTransform2d,
        b: Double,
        zeta: Double): Pair<Double, Double> {

    val posError = positionReference.translation.position - pose.translation.position
    val errorRotation = positionReference.rotation.rotateBy(pose.rotation)
    val angleError = acos(pose.rotation.rotation dot positionReference.rotation.rotation)
    assert(errorRotation.radians == angleError)

    val vd = velocityReference.dx
    val wd = velocityReference.dtheta

    //val velocityReference = velocityReference.copy(dtheta = 0.0) // TODO remove

    //val error = Translation2d(positionReference.translation.position - pose.translation.position)
    //        .rotateByOrigin(Rotation2d.createFromRadians(pose.rotation.radians))

    val k = 2 * zeta * sqrt(wd * wd + b * vd * vd)

    val v = vd * errorRotation.cos + k * posError.x
    val w = wd + b * vd * sinc(angleError) * posError.y + k * angleError

    //val v = velocityReference.dx * cos(angleError) + k * posError.x
    //val omega = velocityReference.dtheta + k * angleError + b * velocityReference.dx * sinc(angleError) * posError.y

    //  v = (vl + vr) / 2 -> forwards velocity (ft/s)
    //  w = (vr - vl) / wb -> angular velocity (rad/s)

    val vl = v - w * Drivetrain.wheelbase
    val vr = v + w * Drivetrain.wheelbase

    return Pair(vl, vr)
}